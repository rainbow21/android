package com.example.android.startandroid;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btnL;
    Button btnR;
    int cur;
    int N = 10;
    Drawable[] imgs;
    int[]   texts;
    Drawable[] areaImgs;

    ImageView img;  // Логотип клуба
    TextView txt;   // Информация о клубе
    ImageView areaImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnL = (Button) findViewById(R.id.btnLeft);
        btnR = (Button) findViewById(R.id.btnRight);
        cur = 0;
        // Создание массива картинок
        imgs = new Drawable[N];
        imgs[0] = getResources().getDrawable(R.drawable.atlant);
        imgs[1] = getResources().getDrawable(R.drawable.lisa);
        imgs[2] = getResources().getDrawable(R.drawable.red_star);
        imgs[3] = getResources().getDrawable(R.drawable.ska);
        imgs[4] = getResources().getDrawable(R.drawable.tifun);
        imgs[5] = getResources().getDrawable(R.drawable.ugra);
        imgs[6] = getResources().getDrawable(R.drawable.avto);
        imgs[7] = getResources().getDrawable(R.drawable.riga);
        imgs[8] = getResources().getDrawable(R.drawable.spartak);
        imgs[9] = getResources().getDrawable(R.drawable.tolpar);

        // Хранятся индексы в ресурсах
        texts = new int[N];
        texts[0] = R.string.txtAtlant;
        texts[1] = R.string.txtLisa;
        texts[2] = R.string.txtRed_star;
        texts[3] = R.string.txtSka;
        texts[4] = R.string.txtTifun;
        texts[5] = R.string.txtUgra;
        texts[6] = R.string.txtAvto;
        texts[7] = R.string.txtRiga;
        texts[8] = R.string.txtSpartak;
        texts[9] = R.string.txtTolpar;


        areaImgs = new Drawable[N];
        areaImgs[0] = getResources().getDrawable(R.drawable.area_atlant);
        areaImgs[1] = getResources().getDrawable(R.drawable.area_lisa);
        areaImgs[2] = getResources().getDrawable(R.drawable.area_red_star);
        //areaImgs[3] = getResources().getDrawable(R.drawable.area_ska);
        //areaImgs[4] = getResources().getDrawable(R.drawable.area_tifun);
        //areaImgs[5] = getResources().getDrawable(R.drawable.area_ugra);

        /*
        for (int i = 0; i < N; i = i + 1)
            areaImgs[i] = getResources().getDrawable(R.drawable.area_atlant);
        */

        // Начальные значения для картинок и сообщений
        img =       (ImageView) findViewById(R.id.img);
        txt =       (TextView)  findViewById(R.id.message);
        areaImg =   (ImageView) findViewById(R.id.areaImg);

        img.setImageDrawable(imgs[cur]);
        txt.setText(texts[cur]);
        areaImg.setImageDrawable(areaImgs[cur]);


        // Объявим общий обработчик нажатия кнопки
        View.OnClickListener clickListner = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (view.getId()){
                    case R.id.btnLeft:
                        cur--;
                        break;
                    case R.id.btnRight:
                        cur++;
                        break;
                }
                // i = 0..5
                if (cur < 0)
                    cur = N-1;
                if (cur > N-1)
                    cur = 0;

                img.setImageDrawable(imgs[cur]);
                txt.setText(texts[cur]);
                areaImg.setImageDrawable(areaImgs[cur]);
            }
        };

        btnL.setOnClickListener(clickListner);
        btnR.setOnClickListener(clickListner);

    }
}
